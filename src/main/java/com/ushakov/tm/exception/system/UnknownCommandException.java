package com.ushakov.tm.exception.system;

import com.ushakov.tm.exception.AbstractException;
import org.jetbrains.annotations.NotNull;

public class UnknownCommandException extends AbstractException {

    public UnknownCommandException(@NotNull String value) {
        super("Error! Unknown command: " + value + "!");
    }

}
