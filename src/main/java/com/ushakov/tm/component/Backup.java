package com.ushakov.tm.component;

import com.ushakov.tm.bootstrap.Bootstrap;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;

public class Backup extends Thread {

    @NotNull
    private final Bootstrap bootstrap;

    private static final int INTERVAL = 30000;

    private static final String BACKUP_SAVE = "backup-save";

    private static final String BACKUP_LOAD = "backup-load";

    public Backup(@NotNull Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
        this.setDaemon(true);
    }

    @Override
    @SneakyThrows
    public void run() {
        while (true) {
            save();
            Thread.sleep(INTERVAL);
        }
    }

    public void init() {
        load();
        start();
    }

    public void save() {
        bootstrap.parseCommand(BACKUP_SAVE);
    }

    public void load() {
        bootstrap.parseCommand(BACKUP_LOAD);
    }

}
