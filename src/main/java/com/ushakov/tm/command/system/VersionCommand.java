package com.ushakov.tm.command.system;

import com.jcabi.manifests.Manifests;
import com.ushakov.tm.command.AbstractCommand;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class VersionCommand extends AbstractCommand {

    @Override
    @Nullable
    public String arg() {
        return "-v";
    }

    @Override
    @Nullable
    public String description() {
        return "Show application version.";
    }

    @Override
    public void execute() {
        System.out.println("[VERSION]");
        System.out.println(Manifests.read("build"));
    }

    @Override
    @NotNull
    public String name() {
        return "version";
    }

}
