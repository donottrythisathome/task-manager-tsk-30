package com.ushakov.tm.command;

import com.ushakov.tm.dto.Domain;
import com.ushakov.tm.exception.empty.EmptyDomainException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public abstract class AbstractDataCommand extends AbstractCommand{

    @NotNull
    protected static final String FILE_BINARY = "./data.bin";

    @NotNull
    protected static final String FILE_BASE64 = "./data.base64";

    @NotNull
    protected static final String FILE_FASTERXML_JSON = "./data-fasterxml.json";

    @NotNull
    protected static final String FILE_FASTERXML_XML = "./data-fasterxml.xml";

    @NotNull
    protected static final String FILE_FASTERXML_YAML = "./data-fasterxml.yaml";

    @NotNull
    protected static final String FILE_JAXB_JSON = "./data-jaxb.json";

    @NotNull
    protected static final String FILE_JAXB_XML = "./data-jaxb.xml";

    @NotNull
    protected static final String JAXB_JSON_PROPERTY_NAME = "eclipselink.media-type";

    @NotNull
    protected static final String JAXB_JSON_PROPERTY_VALUE = "application/json";

    @NotNull
    protected static final String SYSTEM_JSON_PROPERTY_NAME = "javax.xml.bind.context.factory";

    @NotNull
    protected static final String SYSTEM_JSON_PROPERTY_VALUE = "org.eclipse.persistence.jaxb.JAXBContextFactory";

    @NotNull
    protected static final String BACKUP_XML = "./backup-data.xml";

    @NotNull
    public Domain getDomain() {
        @NotNull final Domain domain = new Domain();
        domain.setUsers(serviceLocator.getUserService().findAll());
        domain.setProjects(serviceLocator.getProjectService().findAll());
        domain.setTasks(serviceLocator.getTaskService().findAll());
        return domain;
    }

    public void setDomain(@Nullable Domain domain){
        if (domain == null) throw new EmptyDomainException();
        serviceLocator.getUserService().clear();
        serviceLocator.getProjectService().clear();
        serviceLocator.getTaskService().clear();
        serviceLocator.getUserService().addAll(domain.getUsers());
        serviceLocator.getProjectService().addAll(domain.getProjects());
        serviceLocator.getTaskService().addAll(domain.getTasks());
        serviceLocator.getAuthService().logout();
    }

}
