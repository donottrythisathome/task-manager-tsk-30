package com.ushakov.tm.api.repository;

import com.ushakov.tm.api.IRepository;
import com.ushakov.tm.model.AbstractOwnerEntity;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Comparator;
import java.util.List;

public interface IOwnerRepository<E extends AbstractOwnerEntity> extends IRepository<E> {

    @NotNull
    E add(@NotNull String userId, @NotNull E entity);

    void clear(@NotNull String userId);

    @Nullable
    List<E> findAll(@NotNull String userId);

    @Nullable
    List<E> findAll(@NotNull String userId, @NotNull Comparator<E> comparator);

    @Nullable
    E findOneById(@NotNull String userId, @NotNull String id);

    @Nullable
    E findOneByIndex(@NotNull String userId, @NotNull Integer index);

    @Nullable
    E removeOneById(@NotNull String userId, @NotNull String id);

    @Nullable
    E removeOneByIndex(@NotNull String userId, @NotNull Integer index);

}