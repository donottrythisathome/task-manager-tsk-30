package com.ushakov.tm.api.repository;

import com.ushakov.tm.model.AbstractOwnerBusinessEntity;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public interface IOwnerBusinessRepository<E extends AbstractOwnerBusinessEntity> extends IOwnerRepository<E> {

    @Nullable
    E findOneByName(@NotNull String userId, @NotNull String name);

    @Nullable
    E removeOneByName(@NotNull String userId, @NotNull String name);

}