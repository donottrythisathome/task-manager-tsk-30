package com.ushakov.tm.api.repository;

import com.ushakov.tm.model.Project;


public interface IProjectRepository extends IOwnerBusinessRepository<Project> {
}
