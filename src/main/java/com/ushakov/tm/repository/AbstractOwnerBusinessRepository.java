package com.ushakov.tm.repository;

import com.ushakov.tm.api.repository.IOwnerBusinessRepository;
import com.ushakov.tm.exception.entity.ObjectNotFoundException;
import com.ushakov.tm.model.AbstractOwnerBusinessEntity;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Optional;

public abstract class AbstractOwnerBusinessRepository<E extends AbstractOwnerBusinessEntity>
        extends AbstractOwnerRepository<E>
        implements IOwnerBusinessRepository<E> {

    @Override
    @Nullable
    public E findOneByName(final @NotNull String userId, final @NotNull String name) {
        return list.stream()
                .filter(e -> e.getName().equals(name) && userId.equals(e.getUserId()))
                .findFirst()
                .orElseThrow(ObjectNotFoundException::new);
    }

    @Override
    @Nullable
    public E removeOneByName(@NotNull final String userId, @NotNull final String name) {
        @Nullable final E entity = findOneByName(userId, name);
        Optional.ofNullable(entity).ifPresent(list::remove);
        return entity;
    }

}